package com.apnovator.cancerapp;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

public class SplashActivity extends Activity {
	private static int splash_time_out = 5000;
	String string = "splash";
	SharedPreferences preference;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String langSelection = " ";
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		langSelection = preference.getString("langSelect", " ");
		if (langSelection.compareTo("spanish") == 0) {
			Locale locale2 = new Locale("sp");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		} else if (langSelection.compareTo("english") == 0) {
			Locale locale2 = new Locale("en");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_splash);
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (preference.getBoolean("activity_executed", false)) {
					if (preference.getString("Setting", " ").compareTo(
							"Setting") == 0) {

						Intent i = new Intent(SplashActivity.this,
								HomeMenu.class);
						// i.putExtra("caller",string);
						startActivity(i);
						finish();
					} else if (preference.getBoolean("activity_executed1",
							false)) {
						Intent i = new Intent(SplashActivity.this,
								HomeMenu.class);
						// i.putExtra("caller",string);
						startActivity(i);
						finish();
					} else {
						Intent i = new Intent(SplashActivity.this,
								Splash2Activity.class);
						// i.putExtra("caller",string);
						startActivity(i);
						finish();
					}
				}

			}

		}, splash_time_out);
	}
}
