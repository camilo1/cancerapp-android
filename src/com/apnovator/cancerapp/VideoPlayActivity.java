package com.apnovator.cancerapp;

import java.util.Locale;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoPlayActivity extends Activity {
	SharedPreferences preference;
	private String langSelection = " ";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		langSelection = preference.getString("langSelect", " ");
		if (langSelection.compareTo("spanish") == 0) {
			Locale locale2 = new Locale("sp");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		} else if (langSelection.compareTo("english") == 0) {
			Locale locale2 = new Locale("en");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_video_play);
		//final VideoView videoView1=(VideoView)findViewById(R.id.video_player_view1);
		//final VideoView videoView2=(VideoView)findViewById(R.id.video_player_view2);
		//final MediaController mc = new MediaController(this);
		//MediaController mc1 = new MediaController(this);
		Button play_btn=(Button)findViewById(R.id.play_btn);
		Button play_btn1=(Button)findViewById(R.id.play_btn1);
		//videoView2.setMediaController(mc1);
		final String uri = "android.resource://" + getPackageName() + "/" + R.raw.video_autoexam1;
		//videoView1.setVideoURI(Uri.parse(uri));
		//videoView2.setVideoURI(Uri.parse(uri));

		play_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.v("check"," "+1);
				//videoView1.start();
				Intent i=new Intent(VideoPlayActivity.this,PlayActivity.class);
				startActivity(i);
			}
		});
		play_btn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(VideoPlayActivity.this,PlayActivity.class);
				startActivity(i);
			}
		});
		Typeface type1 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Heavy.otf");
		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Roman.otf");
		TextView videoText2 = (TextView) findViewById(R.id.videoText2);
		TextView videoText1 = (TextView) findViewById(R.id.videoText1);
		final ScrollView videoScroll = (ScrollView) findViewById(R.id.videoScroll);
		videoText1.setTypeface(type1);
		videoText2.setTypeface(type2);
		TextView videoText4 = (TextView) findViewById(R.id.videoText4);
		TextView videoText3 = (TextView) findViewById(R.id.videoText3);
		videoText3.setTypeface(type1);
		videoText4.setTypeface(type2);
		ImageButton downVideo = (ImageButton) findViewById(R.id.downVideo);
		downVideo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				videoScroll.scrollBy(0, 40);
			}
		});
	}
}
