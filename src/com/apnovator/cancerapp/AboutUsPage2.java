package com.apnovator.cancerapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class AboutUsPage2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us_page2);
		Intent intent =getIntent();
		int url=intent.getIntExtra("url",0);
		Log.v("url"," "+url);
		WebView abtus2 = (WebView) findViewById(R.id.abtusweb2);
		abtus2.getSettings().setJavaScriptEnabled(true);
		final Activity activity = this;
		abtus2.setWebViewClient(new WebViewClient() {
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Toast.makeText(activity, description, Toast.LENGTH_SHORT)
						.show();
			}
		});
		if(url==1){
		abtus2.loadUrl("https://www.facebook.com/pages/Modo-Rosa/290423424438377?fref=ts");
		}else if(url==2){
			abtus2.loadUrl("http://instagram.com/modorosa");
		}else if(url==3){
			abtus2.loadUrl("https://twitter.com/modorosa");
		}
	}
}
