package com.apnovator.cancerapp;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import android.app.ActivityManager;
import android.app.ActionBar.LayoutParams;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.Toast;

public class SelfExamAlarmReceive extends BroadcastReceiver {
	MediaPlayer mediaPlayer;
	int message1;
	int message2;
	int message3;
	int message4;
	int alarm1;
	private NotificationManager mNotificationManager;
	private int SIMPLE_NOTFICATION_ID;
	Notification myNotification;
	CharSequence notificationTitle;
	CharSequence notificationText;
	private String langSelection = " ";
	SharedPreferences preference;

	@SuppressWarnings("deprecation")
	@Override
	public void onReceive(Context context, Intent intent) {
		// message=intent.getIntExtra("alarm", 0);
		// Log.v("var"," "+message);
		preference = context.getSharedPreferences("MyPREFERENCES",
				Context.MODE_PRIVATE);
		message1 = preference.getInt("sound1", 0);
		message2 = preference.getInt("sound2", 0);
		message3 = preference.getInt("sound3", 0);
		message4 = preference.getInt("sound4", 0);
		if (langSelection.compareTo("spanish") == 0) {
			Locale locale2 = new Locale("sp");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			context.getResources().updateConfiguration(config,
					context.getResources().getDisplayMetrics());
		} else if (langSelection.compareTo("english") == 0) {
			Locale locale2 = new Locale("en");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			context.getResources().updateConfiguration(config,
					context.getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				context.getResources().updateConfiguration(config,
				context.getResources().getDisplayMetrics());
		}
		// message1 = intent.getIntExtra("message1", 0);
		alarm1 = intent.getIntExtra("alarm", 0);
	
		Date alarmDate = new Date();
		

		myNotification = new NotificationCompat.Builder(context)

		.setTicker("Notification!").setWhen(System.currentTimeMillis())

		.setAutoCancel(true).setSmallIcon(R.drawable.ic_launcher)

		.build();

		if (alarm1 == 1) {
			Intent notificationIntent = new Intent(context, AlarmActivity.class);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent contentIntent = PendingIntent.getActivity(context,
					-1, notificationIntent, 0);
			CharSequence notificationTextSelf = context.getResources()
					.getString(R.string.NotificationtextSelf);
			CharSequence notificationTitleSelf = context.getResources()
					.getString(R.string.SelfTitle);
			myNotification.setLatestEventInfo(context, notificationTitleSelf,
					notificationTextSelf, contentIntent);

			if (message1 == 1) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound1");
			} else if (message1 == 2) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound2");
			} else if (message1 == 3) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound3");
			} else if (message1 == 6) {
				// myNotification.sound=Uri.parse("android.resource://com.apnovator.cancerapp/raw/sound4");
				myNotification.defaults = Notification.DEFAULT_VIBRATE;
			}
			int cycle=intent.getIntExtra("cycle",0);
			int n2 = preference.getInt("alarmType", -1);
			AlarmManager am = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			alarmDate.setTime(preference.getLong("repeatDateSelf", -1));
			Calendar cal = Calendar.getInstance();
			cal.setTime(alarmDate);
			Log.v("Monthly"," "+n2);
			if(n2==1){
				cal.add(Calendar.DATE, cycle);
				Log.v("CysleConfirm"," "+cycle);
			}
			else if(n2==2) {
				
				cal.add(Calendar.MONTH, 1);
				
			}
			// intent.putExtra("sound1", message1);
			
			@SuppressWarnings("static-access")
			Date alarmDateSelf = cal.getTime();
			AlarmManager am1 = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent alarmIntent = new Intent(context, SelfExamAlarmReceive.class);
			intent.putExtra("alarm", alarm1);
			intent.putExtra("alarmFirstType", n2);
						Editor editor = preference.edit();
			editor.putLong("repeatDateSelf", alarmDateSelf.getTime());
			editor.commit();
			PendingIntent pi = PendingIntent
					.getBroadcast(context, 0, intent, 0);
			
			am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);
			// mediaPlayer.start();
		} else if (alarm1 == 2) {
			
			Intent notificationIntent = new Intent(context, AlarmActivity.class);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent contentIntent = PendingIntent.getActivity(context,
					-2, notificationIntent, 0);
			CharSequence notificationTextGyna = context.getResources()
					.getString(R.string.NotificationtextGyna);
			CharSequence notificationTitleGyna = context.getResources()
					.getString(R.string.GynaTitle);
			myNotification.setLatestEventInfo(context, notificationTitleGyna,
					notificationTextGyna, contentIntent);

			if (message2 == 1) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound1");
			} else if (message2 == 2) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound2");
			} else if (message2 == 3) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound3");
			} else if (message2 == 6) {
				// myNotification.sound=Uri.parse("android.resource://com.apnovator.cancerapp/raw/sound4");
				myNotification.defaults = Notification.DEFAULT_VIBRATE;

			}
			alarmDate.setTime(preference.getLong("repeatDateGyna", -1));
			Log.v("confirmAlarm"," "+alarmDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(alarmDate);
			cal.add(Calendar.YEAR, 1);
			Date alarmDateGyna = cal.getTime();
			AlarmManager am1 = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent alarmIntent = new Intent(context, SelfExamAlarmReceive.class);
			// intent.putExtra("sound2", message2);
			intent.putExtra("alarm", alarm1);
			Editor editor = preference.edit();
			editor.putLong("repeatDateGyna", alarmDateGyna.getTime());
			editor.commit();
			@SuppressWarnings("static-access")
			PendingIntent pi = PendingIntent.getBroadcast(context, 22,
					alarmIntent, 0);
			am1.cancel(pi);
			am1.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);

		}

		else if (alarm1 == 3) {
			Intent notificationIntent = new Intent(context, AlarmActivity.class);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent contentIntent = PendingIntent.getActivity(context,
					-3, notificationIntent, 0);
			CharSequence notificationTextMamo = context.getResources()
					.getString(R.string.NotificationtextMamo);
			CharSequence notificationTitleMamo = context.getResources()
					.getString(R.string.MamoTitle);
			myNotification.setLatestEventInfo(context, notificationTitleMamo,
					notificationTextMamo, contentIntent);

			if (message3 == 1) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound1");
			} else if (message3 == 2) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound2");
			} else if (message3 == 3) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound3");
			} else if (message3 == 6) {
				// myNotification.sound=Uri.parse("android.resource://com.apnovator.cancerapp/raw/sound4");
				myNotification.defaults = Notification.DEFAULT_VIBRATE;
			}
			alarmDate.setTime(preference.getLong("repeatDateMamo", -1));
			Log.v("confirmAlarm"," "+alarmDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(alarmDate);
			cal.add(Calendar.YEAR, 1);
			Date alarmDateMamo = cal.getTime();
			AlarmManager am2 = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent alarmIntent = new Intent(context, SelfExamAlarmReceive.class);
			// intent.putExtra("sound3", message3);
			intent.putExtra("alarm", alarm1);
			Editor editor = preference.edit();
			editor.putLong("repeatDateMamo", alarmDateMamo.getTime());
			editor.commit();
			@SuppressWarnings("static-access")
			PendingIntent pi = PendingIntent.getBroadcast(context, 21,
					alarmIntent, 0);
			am2.cancel(pi);
			am2.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);

		} else if (alarm1 == 4) {
			Intent notificationIntent = new Intent(context, AlarmActivity.class);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent contentIntent = PendingIntent.getActivity(context,
					-4, notificationIntent, 0);
			CharSequence notificationTextEco = context.getResources()
					.getString(R.string.NotificationtextMamo);
			CharSequence notificationTitleEco = context.getResources()
					.getString(R.string.EcoTitle);
			myNotification.setLatestEventInfo(context, notificationTitleEco,
					notificationTextEco, contentIntent);

			if (message4 == 1) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound1");
			} else if (message4 == 2) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound2");
			} else if (message4 == 3) {
				myNotification.sound = Uri
						.parse("android.resource://com.apnovator.cancerapp/raw/sound3");
			} else if (message4 == 6) {
				// myNotification.sound=Uri.parse("android.resource://com.apnovator.cancerapp/raw/sound4");
				myNotification.defaults = Notification.DEFAULT_VIBRATE;

			}
			alarmDate.setTime(preference.getLong("repeatDateEco", -1));
			Log.v("confirmAlarm"," "+alarmDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(alarmDate);
			cal.add(Calendar.YEAR, 1);
			Date alarmDateEco = cal.getTime();
			AlarmManager am2 = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent alarmIntent = new Intent(context, SelfExamAlarmReceive.class);
			// intent.putExtra("sound3", message3);
			intent.putExtra("alarm", alarm1);
			Editor editor = preference.edit();
			editor.putLong("repeatDateEco", alarmDateEco.getTime());
			editor.commit();
			@SuppressWarnings("static-access")
			PendingIntent pi = PendingIntent.getBroadcast(context, -1,
					alarmIntent, 0);
			am2.cancel(pi);
			am2.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);
		}

		// TODO Auto-generated method stub
		if (isApplicationSentToBackground(context)) {
			mNotificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(SIMPLE_NOTFICATION_ID, myNotification);

		} else {
			Intent dialogIntent = new Intent(context,
					AlarmRecieverDialogActivity.class);
			dialogIntent.putExtra("alarm1", alarm1);
			dialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(dialogIntent);

		}
	}

	private boolean isApplicationSentToBackground(Context mcontext) {
		// TODO Auto-generated method stub
		ActivityManager am = (ActivityManager) mcontext
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(mcontext.getPackageName())) {
				return true;
			}
		}
		return false;
	}

}
