package com.apnovator.cancerapp;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class SountVideoActivity extends YouTubeBaseActivity implements
YouTubePlayer.OnInitializedListener { 
	public static final String API_KEY = "AIzaSyDy6GR8aYdJD4_57DIuKhpliubeOUOxV1Y";
	public static final String VIDEO_ID = "gjB_0IUdqjA";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sount_video);
		YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
		youTubePlayerView.initialize(API_KEY, this);
	}
	@Override
	public void onInitializationFailure(Provider arg0,
			YouTubeInitializationResult arg1) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG)
		.show();
	}
	@Override
	public void onInitializationSuccess(Provider arg0, YouTubePlayer arg1,
			boolean arg2) {
		// TODO Auto-generated method stub
		arg1.setPlayerStateChangeListener(playerStateChangeListener);
		arg1.setPlaybackEventListener(playbackEventListener);
		if (!arg2) {
			arg1.cueVideo(VIDEO_ID);
			arg1.setShowFullscreenButton(arg2);
	}
	};
		private PlaybackEventListener playbackEventListener = new PlaybackEventListener() {

			@Override
			public void onBuffering(boolean arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPaused() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPlaying() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSeekTo(int arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopped() {
				// TODO Auto-generated method stub
			
			}
			
		};
		private PlayerStateChangeListener playerStateChangeListener = new PlayerStateChangeListener() {

			@Override
			public void onAdStarted() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onError(ErrorReason arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onLoaded(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onLoading() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onVideoEnded() {
				// TODO Auto-generated method stub
				Intent intent=new Intent(SountVideoActivity.this,Monthly_Evaluation.class);	
				startActivity(intent);
				finish();
			}

			@Override
			public void onVideoStarted() {
				// TODO Auto-generated method stub
				
			}
};
}
