package com.apnovator.cancerapp;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class LangSelection extends Activity {
	String message = " ";
	String mssg = "Setting";
	SharedPreferences preference;
	private String langSelect = "langSelect";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		if (preference.getBoolean("activity_executed", false)) {
			Intent intent = new Intent(this, SplashActivity.class);
			startActivity(intent);
			finish();
		} 
		setContentView(R.layout.activity_lang_selection);
		Button english = (Button) findViewById(R.id.English);
		Button french = (Button) findViewById(R.id.french);
		Button purtugu = (Button) findViewById(R.id.purtugu);
		english.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Editor ed = preference.edit();
				ed.putString(langSelect, "english");
				ed.putBoolean("activity_executed", true);
				ed.commit();
				Intent intent;
				if (preference.getString("Setting", " ").compareTo("Setting") == 0) {
					intent = new Intent(LangSelection.this, HomeMenu.class);
					startActivity(intent);
					finish();
				} else {

					intent = new Intent(LangSelection.this,
							SplashActivity.class);
					startActivity(intent);
					finish();
				}
			}
		});
		purtugu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Editor ed = preference.edit();
				ed.putBoolean("activity_executed", true);
				ed.putString(langSelect, "english");
				ed.commit();
				Intent intent;
				if (preference.getString("Setting", " ").compareTo("Setting") == 0) {
					intent = new Intent(LangSelection.this, HomeMenu.class);
					startActivity(intent);
					finish();
				} else {

					intent = new Intent(LangSelection.this,
							SplashActivity.class);
					startActivity(intent);
					finish();
				}
			}
		});

		french.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Editor ed = preference.edit();
				ed.putBoolean("activity_executed", true);
				ed.putString(langSelect, "french");
				ed.commit();
				Intent intent;
				if (preference.getString("Setting", " ").compareTo("Setting") == 0) {
					intent = new Intent(LangSelection.this, HomeMenu.class);
					startActivity(intent);
					finish();
				} else {
					intent = new Intent(LangSelection.this,
							SplashActivity.class);
					startActivity(intent);
					finish();
				}
			}
		});
	}

	public void espanolClick(View view) {
		Editor ed = preference.edit();
		ed.putBoolean("activity_executed", true);
		ed.putString(langSelect, "spanish");
		ed.commit();
		Intent intent;
		if (preference.getString("Setting", " ").compareTo("Setting") == 0) {
			intent = new Intent(LangSelection.this, HomeMenu.class);
			startActivity(intent);
			finish();
		} else {
			intent = new Intent(LangSelection.this, SplashActivity.class);
			startActivity(intent);
			finish();
		}
	}
}
