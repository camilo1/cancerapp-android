package com.apnovator.cancerapp;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class SponsorActivity extends Activity {
	String msg=" ";
	SharedPreferences preference;
	private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_sponsor);
		ImageButton phonebtn=(ImageButton)findViewById(R.id.phonebtn);
		ImageButton malebtn=(ImageButton)findViewById(R.id.malebtn);
		Button id_button=(Button)findViewById(R.id.id_button);
		TextView sponserText=(TextView)findViewById(R.id.sponserText);
		Typeface type2=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Roman.otf");
		id_button.setTypeface(type2);
		sponserText.setTypeface(type2);
		phonebtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:5743525033"));
				startActivity(intent); 
				
			}
		});
		malebtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
						 "mailto", "modorosa@tripartitacomunicaciones.com", null));
				
						 emailIntent.putExtra(Intent.EXTRA_SUBJECT, " ");
						 startActivity(Intent.createChooser(emailIntent, null));
						
			}
	});
		id_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			msg="link5";
				Intent intent=new Intent(SponsorActivity.this,AboutUsPage1.class);	
				intent.putExtra("name",msg);
				startActivity(intent);
			}
	});
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		//Intent in=new Intent(this,HomeMenu.class);
		//startActivity(in);
		//finish();
	}
}
