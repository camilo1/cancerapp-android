package com.apnovator.cancerapp;

import java.util.List;
import java.util.Locale;

import android.R.layout;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Toast;

public class HomeMenu extends Activity {
	Button btnClosePopup;
	private PopupWindow pwindo;
	SharedPreferences preference;
	private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_home_menu);
		
	}

	public void alarmActivity(View v) {
		Intent intentAlarm = new Intent(this, AlarmActivity.class);
		startActivity(intentAlarm);
		
	}

	public void videoActivity(View v) {
		Intent intentAlarm = new Intent(this, VideosActivity.class);
		startActivity(intentAlarm);
	}

	public void aboutUs(View v) {
		Intent intentAlarm = new Intent(this, ModarosaActivity.class);
		startActivity(intentAlarm);
		
	}

	public void shareapp(View v) {

		Intent shareintent = new Intent(Intent.ACTION_SEND);
		LayoutInflater inflater = (LayoutInflater) HomeMenu.this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.share_popup,
				(ViewGroup) findViewById(R.id.popup_element2));
		pwindo = new PopupWindow(layout, LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT, true);
		pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
		btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popupShare);
		btnClosePopup.setOnClickListener(cancel_button_click_listener);
		/*Intent emailIntent = new Intent(Intent.ACTION_SENDTO,Uri.fromParts("mailto", " ", null));
		//emailIntent.setType("text/plain");
		emailIntent.putExtra(Intent.EXTRA_TEXT," The required app to each an every woman http://modorosaapp_itunes ");
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "MODO ROSA App");
		final PackageManager pm = getPackageManager();
		final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
		ResolveInfo best = null;
		for (final ResolveInfo info : matches)
			if (info.activityInfo.packageName.endsWith(".gm")|| info.activityInfo.name.toLowerCase().contains("gmail"))
				best = info;
		if (best != null)
			emailIntent.setClassName(best.activityInfo.packageName,best.activityInfo.name);
		startActivity(emailIntent);*/
	}

	public void selfEvaluation(View v) {
		Intent intentAlarm = new Intent(this, Monthly_Evaluation.class);
		startActivity(intentAlarm);
		
	}

	public void sponsers(View v) {
		Intent intentAlarm = new Intent(this, SettingActivity.class);
		startActivity(intentAlarm);
		
	}

	

	private OnClickListener cancel_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			pwindo.dismiss();
			Intent emailIntent = new Intent(Intent.ACTION_SENDTO,Uri.fromParts("mailto", " ", null));
			//emailIntent.setType("text/plain");
			String emailText=getResources().getString(R.string.emailText);
			String emailSubject=getResources().getString(R.string.emailSubject);
			emailIntent.putExtra(Intent.EXTRA_TEXT,emailText);
			emailIntent.putExtra(Intent.EXTRA_SUBJECT,emailSubject);
			final PackageManager pm = getPackageManager();
			final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
			ResolveInfo best = null;
			for (final ResolveInfo info : matches)
				if (info.activityInfo.packageName.endsWith(".gm")|| info.activityInfo.name.toLowerCase().contains("gmail"))
					best = info;
			if (best != null)
				emailIntent.setClassName(best.activityInfo.packageName,best.activityInfo.name);
			startActivity(emailIntent);	

	}
		// Intent shareintent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
		// "mailto", "", null));
		// Intent shareintent = new Intent(Intent.ACTION_SEND);
		// shareintent.setType("text/plain");
		// shareintent.putExtra(Intent.EXTRA_TEXT," The required app to each an every woman http://modorosaapp_itunes ");
		// shareintent.putExtra(Intent.EXTRA_SUBJECT, "MODO ROSA App");
		// startActivity(Intent.createChooser(shareintent, "Send Email"));
		// startActivity(Intent.createChooser(shareintent, "Share With"));
		// }
//	};
	};

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		int backButtonCount=1;
		if(backButtonCount >= 1)
	    {
	        Intent intent = new Intent(Intent.ACTION_MAIN);
	        intent.addCategory(Intent.CATEGORY_HOME);
	       startActivity(intent);
	       finish();
	    }
		else
	    {
	        Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
	        backButtonCount++;
	    }

	}
}
