package com.apnovator.cancerapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class AppointmentAlarm extends Activity {
	private int nYear;
	private int nMonth;
	private int nDay;
	static final int DATE_DIALOG_ID = 0;
	static final int TIME_DIALOG_ID = 1;
	private int nHour;
	private int nMinute;
	private String setDate = " ";
	private String setDates = " ";
	private String setTime = " ";
	private TextView gynaAlert1;
	private TextView gynaAlert2;
	private ImageButton gynaAlertSave;
	private String n = " ";
	private String n1 = " ";
	SharedPreferences preference;
	public static final String keyGyna = "keyGyna";
	public static final String key3 = "key3";
	private int confirmGyna;
	private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_appointment_alarm);
		gynaAlert1 = (TextView) findViewById(R.id.gynaAlert1);
		Typeface type2=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Roman.otf");
		TextView SetDateText=(TextView)findViewById(R.id.SetDateText);
		SetDateText.setTypeface(type2);
		gynaAlert1.setTypeface(type2);
		if((preference.getString(keyGyna, " ")).compareTo(" ")==0){
			setDefaultDate();
		}
		else{
		 gynaAlert1.setText(preference.getString(keyGyna, " "));
		}
		//gynaAlert2 = (TextView) findViewById(R.id.gynaAlert2);
		Button bck_btn=(Button)findViewById(R.id.bck_btn2);
		bck_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//onBackPressed();
				confirmGyna=1;
				Editor editor1 = preference.edit();
				Intent returnIntent = new Intent();
				
					editor1.putString(keyGyna, gynaAlert1.getText().toString());

					returnIntent.putExtra("appoinment",gynaAlert1.getText().toString());

				 returnIntent.putExtra("confirmGyna", confirmGyna);
				editor1.commit();
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
		gynaAlertSave = (ImageButton) findViewById(R.id.gynaAlertSave);
		

		//gynaAlert1.setText(preference.getString(key2, " "));
		//gynaAlert2.setText(preference.getString(key3, " "));
		
		gynaAlertSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				confirmGyna=1;
				Editor editor1 = preference.edit();
				Intent returnIntent = new Intent();
				
					editor1.putString(keyGyna, gynaAlert1.getText().toString());

					returnIntent.putExtra("appoinment",gynaAlert1.getText().toString());

				 returnIntent.putExtra("confirmGyna", confirmGyna);
				editor1.commit();
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
		/*gynaAlert2.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog(TIME_DIALOG_ID);
				final Calendar c = Calendar.getInstance();
				nHour = c.get(Calendar.HOUR_OF_DAY);
				nMinute = c.get(Calendar.MINUTE);
			}
		});*/

		gynaAlert1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog(DATE_DIALOG_ID);

			}
		});
		final Calendar c = Calendar.getInstance();
		nYear = c.get(Calendar.YEAR);
		nMonth = c.get(Calendar.MONTH);
		nDay = c.get(Calendar.DAY_OF_MONTH);
	
	/*String	DefaultDate=nDay + "/" + nMonth + "/" + nYear;
		 if (preference.getString(keyGyna, " ")==" ") {
			 gynaAlert1.setText(DefaultDate);
				 Log.v("default",DefaultDate);
			}
			else{
				 Log.v("default",preference.getString(keyGyna, " "));
				 gynaAlert1.setText(preference.getString(keyGyna, " "));
			}*/
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, nDateSetListener, nYear, nMonth,
					nDay);
		case TIME_DIALOG_ID:
			return new TimePickerDialog(this, nTimeSetListener, nHour, nMinute,
					false);
		}
		return null;
	}

	private void updateDisplay() {
		setDate = nDay + "/" + nMonth + "/" + nYear;
		gynaAlert1.setText(setDate);
	}

	private DatePickerDialog.OnDateSetListener nDateSetListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			nYear = year;
			nMonth = monthOfYear + 1;
			nDay = dayOfMonth;
			updateDisplay();

			n = gynaAlert1.getText().toString();
			// n=setDate;
		}
	};
	private TimePickerDialog.OnTimeSetListener nTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			nHour = hourOfDay;
			nMinute = minute;
			setTime = nHour + ":" + nMinute;
			gynaAlert2.setText(setTime);
			n1 = gynaAlert2.getText().toString();

		}
	};

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	public void setDefaultDate() {
		Calendar cal = Calendar.getInstance();
		nYear = cal.get(Calendar.YEAR);
		nMonth = cal.get(Calendar.MONTH);
		nMonth=nMonth+1;
		nDay = cal.get(Calendar.DAY_OF_MONTH);
		updateDisplay();
		n = gynaAlert1.getText().toString();
	}
}