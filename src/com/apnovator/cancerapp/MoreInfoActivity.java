package com.apnovator.cancerapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MoreInfoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more_info);
		WebView moreinfoview = (WebView) findViewById(R.id.moreinfoview);
		moreinfoview.getSettings().setJavaScriptEnabled(true);
		final Activity activity = this;
		moreinfoview.setWebViewClient(new WebViewClient() {
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Toast.makeText(activity, description, Toast.LENGTH_SHORT)
						.show();
			}
		});
		moreinfoview.loadUrl("http://www.1tucan.com");
	}
}
