package com.apnovator.cancerapp;

import java.util.List;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class MamographyAlertReceiver extends BroadcastReceiver {
	private NotificationManager aNotificationManager;
	private int SIMPLE_NOTFICATION_ID1;
	Notification aNotification;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		aNotification = new NotificationCompat.Builder(context)
				.setContentTitle("Alarm Notification!")
				.setContentText("http://android-er.blogspot.com/")
				.setTicker("Notification!")
				.setWhen(System.currentTimeMillis())
				// .setContentIntent(pendingIntent)
				.setDefaults(Notification.DEFAULT_SOUND).setAutoCancel(true)
				.setSmallIcon(R.drawable.ic_launcher)
				.setDefaults(Notification.DEFAULT_VIBRATE).build();
		if (isApplicationSentToBackground(context)) {
			aNotificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			aNotificationManager.notify(SIMPLE_NOTFICATION_ID1, aNotification);
		} else {
			Toast.makeText(context, "Alarm set", Toast.LENGTH_LONG).show();
			Vibrator vibrator = (Vibrator) context
					.getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(2000);

		}
	}

	private boolean isApplicationSentToBackground(Context mcontext) {
		// TODO Auto-generated method stub
		ActivityManager am = (ActivityManager) mcontext
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(mcontext.getPackageName())) {
				return true;
			}
		}
		return false;
	}

}
