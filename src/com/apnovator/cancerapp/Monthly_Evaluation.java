package com.apnovator.cancerapp;

import java.io.IOException;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Layout;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class Monthly_Evaluation extends Activity {
	private int getbutton;
	private MediaPlayer mMediaPlayer;
	private Boolean isPlaying = false;
	ScrollView selfExamScroll;
	LinearLayout layout1;
	Boolean flag = false;
	SharedPreferences preference;
	private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.evaluation_design);
		 Display display = getWindowManager().getDefaultDisplay();
		TextView monthlyTextView = (TextView) findViewById(R.id.monthlyTextView);
		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Roman.otf");
		int height = display.getHeight();
		Log.v("height"," "+height);
		
			height=(int)(0.9*display.getHeight()) ;
			LinearLayout selflayout=(LinearLayout)findViewById(R.id.self_layout);
			LayoutParams params = selflayout.getLayoutParams();
			if (params == null) { 
			    params = new LayoutParams(LayoutParams.MATCH_PARENT, height); 
			} else {
			    params.height = height;
			}

			selflayout.setLayoutParams(params);
		
			// initialize new parameters for my element
			//selflayout.setLayoutParams(new LinearLayout.LayoutParams(params));
		
		// int width = (int) (display.getWidth());
		monthlyTextView.setTypeface(type2);
		Button ebtn1 = (Button) findViewById(R.id.evaluation_1);
		Button ebtn2 = (Button) findViewById(R.id.evaluation_2);
		Button ebtn3 = (Button) findViewById(R.id.evaluation_3);
		Button ebtn4 = (Button) findViewById(R.id.evaluation_4);
		Button ebtn5 = (Button) findViewById(R.id.evaluation_5);
		selfExamScroll = (ScrollView) findViewById(R.id.selfExamScroll);
		ImageButton scroll_btn = (ImageButton) findViewById(R.id.scroll_btn1);
		// layout1 = (LinearLayout) findViewById(R.id.layout1);
		// layout1.setLayoutParams(new LayoutParams(width, height));

		scroll_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// layout1.scrollTo(0,50);

				if (flag == false) {
					selfExamScroll.fullScroll(ScrollView.FOCUS_DOWN);
					flag = true;
				} else {
					selfExamScroll.fullScroll(ScrollView.FOCUS_UP);
					flag = false;
				}
			}
		});
		ImageButton btn_Sound = (ImageButton) findViewById(R.id.btn_Sound);
		mMediaPlayer = MediaPlayer
				.create(Monthly_Evaluation.this, R.raw.sound1);
		btn_Sound.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				Intent intent = new Intent(Monthly_Evaluation.this,
						PlayActivity.class);
				
				startActivity(intent);
			}
		});
		ebtn1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getbutton = 0;
				Intent intent = new Intent(Monthly_Evaluation.this,
						Evaluation_stepActivity.class);
				intent.putExtra("name", getbutton);
				startActivity(intent);

			}
		});
		ebtn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getbutton = 1;
				Intent intent = new Intent(Monthly_Evaluation.this,
						Evaluation_stepActivity.class);
				intent.putExtra("name", getbutton);
				startActivity(intent);

			}
		});
		ebtn3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getbutton = 2;
				Intent intent = new Intent(Monthly_Evaluation.this,
						Evaluation_stepActivity.class);
				intent.putExtra("name", getbutton);
				startActivity(intent);

			}
		});
		ebtn4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getbutton = 3;
				Intent intent = new Intent(Monthly_Evaluation.this,
						Evaluation_stepActivity.class);
				intent.putExtra("name", getbutton);
				startActivity(intent);

			}
		});
		ebtn5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getbutton = 4;
				Intent intent = new Intent(Monthly_Evaluation.this,
						Evaluation_stepActivity.class);
				intent.putExtra("name", getbutton);
				startActivity(intent);

			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			mMediaPlayer.stop();
			mMediaPlayer.release();
		}
		Intent in = new Intent(this, HomeMenu.class);
		startActivity(in);
		finish();

	}
}
