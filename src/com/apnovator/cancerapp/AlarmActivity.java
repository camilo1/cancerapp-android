package com.apnovator.cancerapp;

import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;

public class AlarmActivity extends Activity {
private int alarmId;
Button btnClosePopup;
private PopupWindow pwindo;
SharedPreferences preference;
private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_alarm);
		ImageButton examalert=(ImageButton)findViewById(R.id.btn1);
		ImageButton appointmentAlert=(ImageButton)findViewById(R.id.btn2);
		ImageButton mamographyAlert=(ImageButton)findViewById(R.id.btn3);
		ImageButton loveSomeOne=(ImageButton)findViewById(R.id.btn4);
		ImageButton ecography=(ImageButton)findViewById(R.id.btn5);
		ecography.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alarmId=4;
				Intent intent=new Intent(AlarmActivity.this,SelfExamActivity.class);
				intent.putExtra("alarmId", alarmId);
				startActivity(intent);
			}
			
		});
		loveSomeOne.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent shareintent = new Intent(Intent.ACTION_SEND);
				LayoutInflater inflater = (LayoutInflater) AlarmActivity.this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.alarm_popup,
						(ViewGroup) findViewById(R.id.popup_element2));
				pwindo = new PopupWindow(layout, LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT, true);
				pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
				btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup1);
				btnClosePopup.setOnClickListener(cancel_button_click_listener);
				
			}
		});
		mamographyAlert.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alarmId=3;
				Intent intent=new Intent(AlarmActivity.this,SelfExamActivity.class);
				intent.putExtra("alarmId", alarmId);
				startActivity(intent);
				
			}
		});
		 appointmentAlert.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alarmId=2;
				Intent intent=new Intent(AlarmActivity.this,SelfExamActivity.class);
				intent.putExtra("alarmId", alarmId);
				startActivity(intent);
			
			}
			 });
		examalert.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alarmId=1;
			Intent intent=new Intent(AlarmActivity.this,SelfExamActivity.class);
			intent.putExtra("alarmId", alarmId);
			startActivity(intent);
			
			}
		});
	}
	private OnClickListener cancel_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			pwindo.dismiss();
			/*Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
					 "mailto", "abc@gmail.com", null));
					 emailIntent.putExtra(Intent.EXTRA_SUBJECT, " because I love and care about you");
					 emailIntent.putExtra(Intent.EXTRA_TEXT," because I  love and care about you, I'm sending you this email so you don't forget to set your doctors appointment: let's make sure breast cancer is always detected in time!" +"\n"+
					 		" let's add at the end: PS for all the women we care about you can spread the word about");
					 startActivity(Intent.createChooser(emailIntent, null));	*/
			Intent emailIntent = new Intent(Intent.ACTION_SENDTO,Uri.fromParts("mailto", " ", null));
			//emailIntent.setType("text/plain");
			String emailText=getResources().getString(R.string.emailTextAlarm);
			String emailSubject=getResources().getString(R.string.emailSubjectAlarm);
			emailIntent.putExtra(Intent.EXTRA_TEXT,emailText);
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, emailSubject);
			final PackageManager pm = getPackageManager();
			final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
			ResolveInfo best = null;
			for (final ResolveInfo info : matches)
				if (info.activityInfo.packageName.endsWith(".gm")|| info.activityInfo.name.toLowerCase().contains("gmail"))
					best = info;
			if (best != null)
				emailIntent.setClassName(best.activityInfo.packageName,best.activityInfo.name);
			startActivity(emailIntent);	
		}
	};
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent in=new Intent(this,HomeMenu.class);
		startActivity(in);
		finish();
	}
	
}
