package com.apnovator.cancerapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class MamographyAlert extends Activity {
	private int aYear;
	private int aMonth;
	private int aDay;
	static final int DATE_DIALOG_ID = 0;
	static final int TIME_DIALOG_ID = 1;
	private int aHour;
	private int aMinute;
	private String setDate = " ";
	private String setDat = " ";
	private String setTime = " ";
	private TextView mamoAlert1;
	private TextView mamoAlert2;
	SharedPreferences preference;
	public static final String keyMamo = "keyMamo";
	//public static final String key1 = "key1";
	private int mamoAlarmId;
	private String n = " ";
	private String n1 = " ";
	int confirmMamo;
	
	private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_mamography_alert);
		mamoAlert1 = (TextView) findViewById(R.id.mamoAlert1);
		TextView dateTextView1=(TextView)findViewById(R.id.dateTextView1);
		//mamoAlert2 = (TextView) findViewById(R.id.mamoAlert2);
		Intent i = getIntent();
		//mamoAlarmId = i.getIntExtra("appointment", 0);
		// mamoAlert2 = (TextView) findViewById(R.id.mamoAlert2);
		Typeface type2=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Roman.otf");
		mamoAlert1.setTypeface(type2);
		dateTextView1.setTypeface(type2);
		mamoAlert1.setTypeface(type2);
		if(preference.getString(keyMamo, " ").compareTo(" ")==0){
			setDefaultDate();
		}else{
		mamoAlert1.setText(preference.getString(keyMamo, " "));
		}
		/*
		 * if (preference2.contains(key1)) {
		 * mamoAlert2.setText(preference2.getString(key1, " "));
		 * 
		 * } Button bck_btn=(Button)findViewById(R.id.bck_btn);
		 * bck_btn.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Intent in=new
		 * Intent(MamographyAlert.this,SelfExamActivity.class);
		 * startActivity(in); finish(); } });
		 */

		/*
		 * mamoAlert2.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub showDialog(TIME_DIALOG_ID); final Calendar c =
		 * Calendar.getInstance(); aHour = c.get(Calendar.HOUR_OF_DAY); aMinute
		 * = c.get(Calendar.MINUTE); }
		 * 
		 * });
		 */
		Button bck_btn1=(Button)findViewById(R.id.bck_btn1);
		bck_btn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//onBackPressed();
				confirmMamo=1;
				
				Intent returnIntent = new Intent();
				
					returnIntent.putExtra("mamoDateString",mamoAlert1.getText().toString());

				 returnIntent.putExtra("confirmMamo", confirmMamo);
				
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
		mamoAlert1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog(DATE_DIALOG_ID);

			}
		});
		final Calendar c = Calendar.getInstance();
		aYear = c.get(Calendar.YEAR);
		aMonth = c.get(Calendar.MONTH);
		aDay = c.get(Calendar.DAY_OF_MONTH);
		/*String DefaultDate=aDay + "/" + aMonth + "/" + aYear;
		if (preference.getString(keyMamo, " ")==" ") {
			mamoAlert1.setText(DefaultDate);
		}else{
			mamoAlert1.setText(preference.getString(keyMamo, " "));
		}*/
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	public void mamographyAlert(View v) {
		confirmMamo=1;
		
		Intent returnIntent = new Intent();
		
			returnIntent.putExtra("mamoDateString",mamoAlert1.getText().toString());

		 returnIntent.putExtra("confirmMamo", confirmMamo);
		
		setResult(RESULT_OK, returnIntent);
		finish();
		/*
		 * Context context = getApplicationContext(); SimpleDateFormat
		 * dateFormat = new SimpleDateFormat("dd/MM/yyyy"); Date mamodate = new
		 * Date();
		 * 
		 * 
		 * try { mamodate = dateFormat.parse(setDate); } catch
		 * (java.text.ParseException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * mamodate.setHours(aHour); mamodate.setMinutes(aMinute); Log.v("date",
		 * (mamodate).toString()); Calendar cal = Calendar.getInstance();
		 * cal.setTime(mamodate); cal.add(Calendar.YEAR, 1); if (alarm2 != null)
		 * { AlarmManager am = (AlarmManager) context
		 * .getSystemService(Context.ALARM_SERVICE); Intent intent = new
		 * Intent(context, MamographyAlertReceiver.class); PendingIntent pi =
		 * PendingIntent.getBroadcast(context, 21, intent, 0);
		 * am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000
		 * * 60 * 60 * 24 * 30, pi); Toast.makeText(context,
		 * "Alarm is set for your phone", Toast.LENGTH_SHORT).show(); Editor
		 * editor1 = preference2.edit(); editor1.putString(key,
		 * (mamoAlert1.getText()).toString()); editor1.putString(key1,
		 * (mamoAlert2.getText()).toString()); editor1.commit(); } else {
		 * Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
		 * 
		 * } Log.v("keys", n); Intent intent = new Intent(MamographyAlert.this,
		 * AlarmActivity.class); startActivity(intent); finish();
		 */
	} 

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, aDateSetListener, aYear, aMonth,
					aDay);
		case TIME_DIALOG_ID:
			return new TimePickerDialog(this, aTimeSetListener, aHour, aMinute,
					false);
		}
		return null;
	}

	private void updateDisplay() {
		setDate = aDay + "/" + aMonth + "/" + aYear;
		mamoAlert1.setText(setDate);
	
			
	}

	private DatePickerDialog.OnDateSetListener aDateSetListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			aYear = year;
			aMonth = monthOfYear + 1;
			aDay = dayOfMonth;
			updateDisplay();
			Editor editor1 = preference.edit();
			editor1.putString(keyMamo, setDate);
			editor1.commit();
			n = setDate;

		}
	};
	private TimePickerDialog.OnTimeSetListener aTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			aHour = hourOfDay;
			aMinute = minute;
			setTime = aHour + ":" + aMinute;
			mamoAlert2.setText(setTime);
			n1 = setTime;

		}

	};
	
	public void setDefaultDate() {
		Calendar cal = Calendar.getInstance();
		aYear = cal.get(Calendar.YEAR);
		aMonth = cal.get(Calendar.MONTH);
		aMonth=aMonth+1;
		aDay = cal.get(Calendar.DAY_OF_MONTH);
		updateDisplay();
		Editor editor1 = preference.edit();
		editor1.putString(keyMamo, setDate);
		editor1.commit();
		n = setDate;
	}

}
