package cancerapp.database;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName = "SelfExamAlert")
public class SelfExamAlert {
	@DatabaseField
private String alertTone;
	@DatabaseField
private String repeatInterval;
@DatabaseField
private Date setAlertDate;
	@DatabaseField
private String time;
	public SelfExamAlert(){
		super();
	}
	public SelfExamAlert(String alertTone,String repeatInterval,Date setAlertDate,String time){
		super();
		this.alertTone=alertTone;
		this.repeatInterval=repeatInterval;
		this.setAlertDate=setAlertDate;
		this.time=time;
	}
	public String getAlertTone(){
		return alertTone;
	}
	public void setAlertTone(String alertTone){
		this.alertTone=alertTone;
	}
	public String getRepeatInterval(){
		return  repeatInterval;
	}
	public void setRepeatInterval(String  repeatInterval){
		this. repeatInterval= repeatInterval;
	}
	public Date getsetAlertDate(){
		return setAlertDate;
	}
	public void setSetAlertDate(Date setAlertDate){
		this.setAlertDate=setAlertDate;
	}
	public String getTime(){
		return time;
	}
	public void setTime(String time){
		this.time=time;
	}
}
